//
//  UATTestView.m
//  Testeronium
//
//  Created by Artem Ustimov on 29/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATTestResultViewController.h"

@interface UATTestResultViewController ()

@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UIButton *mainMenuButton;

@end

@implementation UATTestResultViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        UINavigationItem * navigationItem = self.navigationItem;
        navigationItem.title = @"Test Result";
    }
    return self;
}

- (IBAction)mainMenuAction:(id)sender {
    NSLog(@"Main menu clicked!");
}

@end
