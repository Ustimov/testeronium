//
//  UATBaseStore.h
//  Testeronium
//
//  Created by Artem Ustimov on 29/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UATBaseStore : NSObject

@property (nonatomic, readonly) NSArray * objects;

+ (instancetype)sharedStore;
- (NSObject *)createObject;
- (void)removeObject:(NSObject *)object;

@end
