//
//  AppDelegate.h
//  Testeronium
//
//  Created by Artem Ustimov on 25/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UATAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
