//
//  UATBaseStore.m
//  Testeronium
//
//  Created by Artem Ustimov on 29/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATBaseStore.h"

@interface UATBaseStore ()

@property (nonatomic) NSMutableArray * privateObjects;

@end

@implementation UATBaseStore

+ (instancetype)sharedStore {
    static UATBaseStore * sharedStore = nil;
    if (!sharedStore) {
        sharedStore = [[self alloc] initPrivate];
    }
    return sharedStore;
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Singleton" reason:@"Use sharedStore" userInfo:nil];
}

- (instancetype)initPrivate {
    self = [super init];
    if (self) {
        _privateObjects = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray *)objects {
    return self.privateObjects;
}

- (NSObject *)createObject {
    NSObject * object = [[NSObject alloc] init];
    [self.privateObjects addObject:object];
    return object;
}

- (void)removeObject:(NSObject *)object {
    [self.privateObjects removeObjectIdenticalTo:object];
}


@end
