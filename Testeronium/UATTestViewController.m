//
//  UATTestsViewController.m
//  Testeronium
//
//  Created by Artem Ustimov on 26/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATTestViewController.h"
#import "UATTestTableViewController.h"
#import "UATTestResultViewController.h"
#import "UATTestEditViewController.h"
#import "UATTest.h"

@interface UATTestViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITextField *answerTextView;
@property (weak, nonatomic) IBOutlet UIButton *answerButton;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;

@end

@implementation UATTestViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        UINavigationItem * navigationItem = self.navigationItem;
        navigationItem.title = @"Test Detail";

    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //UATTest * test = self.test;
    
    // Init fields form test
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Clear first responder
    [self.view endEditing:YES];
    
    // Set new values to item
}

- (IBAction)answerClick:(id)sender {
    NSLog(@"Answer clicked!");
}

- (IBAction)editTest:(id)sender {
    UATTestEditViewController * testEditViewController = [[UATTestEditViewController alloc] init];
    
    [self.navigationController pushViewController:testEditViewController animated:YES];
}

- (IBAction)completeClick:(id)sender {
    UATTestResultViewController * testResultViewController = [[UATTestResultViewController alloc] init];
    
    [self.navigationController pushViewController:testResultViewController animated:YES];
}

@end
