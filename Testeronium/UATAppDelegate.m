//
//  AppDelegate.m
//  Testeronium
//
//  Created by Artem Ustimov on 25/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATAppDelegate.h"
#import "UATTranslateViewController.h"
#import "UATHistoryTableViewController.h"
#import "UATTestViewController.h"
#import "UATTestTableViewController.h"
#import "UATTranslateTableViewController.h"

@interface UATAppDelegate ()

@end

@implementation UATAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UATTranslateTableViewController * translateTableViewController = [[UATTranslateTableViewController alloc] init];
    UATHistoryTableViewController * historyViewController = [[UATHistoryTableViewController alloc] init];
    UATTestTableViewController * testTableViewController = [[UATTestTableViewController alloc] init];
    
    
    UINavigationController * translateNavigationController = [[UINavigationController alloc] initWithRootViewController:translateTableViewController];
    translateNavigationController.tabBarItem.title = @"Translate";
    
    UINavigationController * historyNavigationController = [[UINavigationController alloc] initWithRootViewController:historyViewController];
    historyNavigationController.tabBarItem.title = @"History";
    
    UINavigationController * testNavigationController = [[UINavigationController alloc] initWithRootViewController:testTableViewController];
    testNavigationController.tabBarItem.title = @"Tests";
    
    UITabBarController * tabBarController = [[UITabBarController alloc] init];
    tabBarController.viewControllers = @[translateNavigationController, historyNavigationController, testNavigationController];
    
    self.window.rootViewController = tabBarController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
