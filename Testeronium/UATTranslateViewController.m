//
//  ViewController.m
//  Testeronium
//
//  Created by Artem Ustimov on 25/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATTranslateViewController.h"
#import "UATTranslateView.h"

@interface UATTranslateViewController ()

@end

@implementation UATTranslateViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.tabBarItem.title = @"Translate";
        
        /*
        UIImage * image = [UIImage imageNamed:@"Translate.png"];
        self.tabBarItem.image = image;
        */
    }
    return self;
}

- (void)loadView {
    CGRect frame = [[UIScreen mainScreen] bounds];
    UATTranslateView * translateView = [[UATTranslateView alloc] initWithFrame:frame];
    self.view = translateView;
}

@end
