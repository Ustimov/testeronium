//
//  UATTranslateView.m
//  Testeronium
//
//  Created by Artem Ustimov on 27/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATTranslateView.h"

@interface UATTranslateView () <UISearchBarDelegate>

@end

@implementation UATTranslateView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CGRect searchBarRect = CGRectMake(40, 70, 240, 30);
        UISearchBar * searchBar = [[UISearchBar alloc] initWithFrame:searchBarRect];
        searchBar.delegate = self;
        [self addSubview:searchBar];
    }
    return self;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Translating started.");
}

@end
