//
//  UATWordTableViewController.m
//  Testeronium
//
//  Created by Artem Ustimov on 27/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATTranslateTableViewController.h"

@interface UATTranslateTableViewController () <UISearchBarDelegate>

@end

@implementation UATTranslateTableViewController

- (instancetype)init {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        UINavigationItem * navigationItem = self.navigationItem;
        navigationItem.title = @"Translate";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    
    // Why adjusted?
    CGRect searchBarRect = CGRectMake(40, 70, 240, 30);
    UISearchBar * searchBar = [[UISearchBar alloc] initWithFrame:searchBarRect];
    searchBar.delegate = self;
    searchBar.keyboardAppearance = UIKeyboardAppearanceLight;
    
    [self.tableView setTableHeaderView:searchBar];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Translating started.");
}
 
@end
