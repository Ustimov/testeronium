//
//  UATTestsViewController.h
//  Testeronium
//
//  Created by Artem Ustimov on 26/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UATTest;

@interface UATTestViewController : UIViewController

@property (nonatomic, strong) UATTest * test;

@end
