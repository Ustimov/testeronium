//
//  UATTestEditViewController.m
//  Testeronium
//
//  Created by Artem Ustimov on 29/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATTestEditViewController.h"

@interface UATTestEditViewController ()
@property (weak, nonatomic) IBOutlet UITextField *testNameField;

@end

@implementation UATTestEditViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        UINavigationItem * navigationItem = self.navigationItem;
        navigationItem.title = @"Test Edit";
    }
    return self;
}

- (IBAction)editWords:(id)sender {
    NSLog(@"Edit words");
}

@end
