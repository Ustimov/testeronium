//
//  UATWordViewController.m
//  Testeronium
//
//  Created by Artem Ustimov on 29/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATWordViewController.h"

@interface UATWordViewController ()

@end

@implementation UATWordViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        UINavigationItem * navigationItem = self.navigationItem;
        navigationItem.title = @"Word Detail";
    }
    return self;
}

@end
