//
//  UATTestsTableViewController.m
//  Testeronium
//
//  Created by Artem Ustimov on 28/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATTestTableViewController.h"
#import "UATTestViewController.h"
#import "UATTestStore.h"
#import "UATTest.h"

@interface UATTestTableViewController ()

@property (nonatomic, strong)IBOutlet UIView * headerView;

@end

@implementation UATTestTableViewController

- (instancetype)init {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        UINavigationItem * navigationItem = self.navigationItem;
        navigationItem.title = @"Tests";
        
        UIBarButtonItem * addBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewTest:)];
        
        navigationItem.rightBarButtonItem = addBarButtonItem;
        navigationItem.leftBarButtonItem = self.editButtonItem;
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[UATTestStore sharedStore] tests] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
    cell.textLabel.text = @"UATTest";
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UATTestViewController * testDetailViewController = [[UATTestViewController alloc] init];
    
    NSArray * tests = [[UATTestStore sharedStore] tests];
    UATTest * selectedTest = tests[indexPath.row];
    
    testDetailViewController.test = selectedTest;
    
    [self.navigationController pushViewController:testDetailViewController animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (IBAction)addNewTest:(id)sender {
    UATTest * newTest = [[UATTestStore sharedStore] createTest];
    
    NSInteger lastRow = [[[UATTestStore sharedStore] tests] indexOfObject:newTest];
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:lastRow inSection:0];
    
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSArray * tests = [[UATTestStore sharedStore] tests];
        UATTest * test = tests[indexPath.row];
        [[UATTestStore sharedStore] removeTest:test];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    
    // Why adjusted?
    CGRect searchBarRect = CGRectMake(40, 70, 240, 30);
    UISearchBar * searchBar = [[UISearchBar alloc] initWithFrame:searchBarRect];
    
    [self.tableView setTableHeaderView:searchBar];
}

@end
