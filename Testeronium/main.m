//
//  main.m
//  Testeronium
//
//  Created by Artem Ustimov on 25/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UATAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UATAppDelegate class]));
    }
}
