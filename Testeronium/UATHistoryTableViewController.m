//
//  UATHistoryViewController.m
//  Testeronium
//
//  Created by Artem Ustimov on 26/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATHistoryTableViewController.h"
#import "UATWordViewController.h"
#import "UATWordStore.h"
#import "UATWord.h"

@interface UATHistoryTableViewController ()

@end

@implementation UATHistoryTableViewController

- (instancetype)init {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        UINavigationItem * navigationItem = self.navigationItem;
        navigationItem.title = @"History";
        
        UIBarButtonItem * addBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewTest:)];
        
        navigationItem.rightBarButtonItem = addBarButtonItem;
        navigationItem.leftBarButtonItem = self.editButtonItem;
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[UATWordStore sharedStore] objects] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
    cell.textLabel.text = @"UATWord";
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UATWordViewController * wordViewController = [[UATWordViewController alloc] init];
    
    //NSArray * words = [[UATWordStore sharedStore] objects];
    //UATWord * selectedWord = words[indexPath.row];
    
    //wordViewController.word = selectedWord;
    
    [self.navigationController pushViewController:wordViewController animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (IBAction)addNewTest:(id)sender {
    NSObject * newWord = [[UATWordStore sharedStore] createObject];
    
    NSInteger lastRow = [[[UATWordStore sharedStore] objects] indexOfObject:newWord];
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:lastRow inSection:0];
    
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSArray * words = [[UATWordStore sharedStore] objects];
        UATWord * word = words[indexPath.row];
        [[UATWordStore sharedStore] removeObject:word];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    
    // Why adjusted?
    CGRect searchBarRect = CGRectMake(40, 70, 240, 30);
    UISearchBar * searchBar = [[UISearchBar alloc] initWithFrame:searchBarRect];
    
    [self.tableView setTableHeaderView:searchBar];
}

/*
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.tabBarItem.title = @"History";
        
         UIImage * image = [UIImage imageNamed:@"History.png"];
         self.tabBarItem.image = image;
 
    }
    return self;
}
*/

@end
