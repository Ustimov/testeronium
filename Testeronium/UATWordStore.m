//
//  UATWordStore.m
//  Testeronium
//
//  Created by Artem Ustimov on 27/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATWordStore.h"
#import "UATWord.h"

@interface UATBaseStore ()

@property (nonatomic) NSMutableArray * privateObjects;

@end

@implementation UATWordStore

- (UATWord *)createObject {
    UATWord * object = [[UATWord alloc] init];
    [self.privateObjects addObject:object];
    return object;
}

@end
