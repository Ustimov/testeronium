//
//  UATTestStore.h
//  Testeronium
//
//  Created by Artem Ustimov on 27/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UATTest;

@interface UATTestStore : NSObject

@property (nonatomic, readonly) NSArray * tests;

+ (instancetype)sharedStore;
- (UATTest *)createTest;
- (void)removeTest:(UATTest *)test;

@end
