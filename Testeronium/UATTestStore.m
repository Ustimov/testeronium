//
//  UATTestStore.m
//  Testeronium
//
//  Created by Artem Ustimov on 27/10/15.
//  Copyright © 2015 Artem Ustimov. All rights reserved.
//

#import "UATTestStore.h"
#import "UATTest.h"

@interface UATTestStore ()

@property (nonatomic) NSMutableArray * privateTests;

@end

@implementation UATTestStore

+ (instancetype)sharedStore {
    static UATTestStore * sharedStore = nil;
    if (!sharedStore) {
        sharedStore = [[self alloc] initPrivate];
    }
    return sharedStore;
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Singleton" reason:@"Use + [UATTestStore sharedStore]" userInfo:nil];
}

- (instancetype)initPrivate {
    self = [super init];
    if (self) {
        _privateTests = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray *)tests {
    return self.privateTests;
}

- (UATTest *)createTest {
    UATTest * test = [[UATTest alloc] init];
    [self.privateTests addObject:test];
    return test;
}

- (void)removeTest:(UATTest *)test {
    [self.privateTests removeObjectIdenticalTo:test];
}

@end
